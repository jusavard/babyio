<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include 'settings.php';
function connect ($servername, $username, $password, $dbname)
{
	// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	// Check connection
	if (!$conn) 
	{
    		die("Connection failed: " . mysqli_connect_error());
	}
	return $conn;
}
function disconnect ($conn)
{
	mysqli_close($conn);
}
function generateTable($conn, $arch, $pkgsrc, $nom_os, $status)
{
	//$sql = "SELECT * from package WHERE nom_arch = \"$arch\" AND nom_source = \"$pkgsrc\" AND (no_status = 1 OR no_status = 2 OR no_status = 3 OR no_status = 4 OR no_status = 5 OR no_status = 6) ORDER BY no_status;";
	echo "SELECT package.no_package,package.nom_package,package.nom_source,package.category_package,package.no_status,package.nom_source,package.nom_arch,os.nom_os,package.priority_package from package,os WHERE package.nom_arch = \"$arch\" AND package.nom_source = \"$pkgsrc\" AND os.nom_os = \"$nom_os\" AND package.no_status = $status ORDER BY package.no_status;";
	//$sql = "SELECT * from package WHERE nom_arch = \"$arch\" AND nom_source = \"$pkgsrc\" AND id_os = $code_os AND no_status = $status ORDER BY no_status;";
	$sql = "SELECT package.no_package,package.nom_package,package.nom_source,package.category_package,package.no_status,package.nom_source,package.nom_arch,os.nom_os,package.priority_package from package,os WHERE package.nom_arch = \"$arch\" AND package.nom_source = \"$pkgsrc\" AND os.nom_os = \"$nom_os\" AND package.no_status = $status ORDER BY package.no_status;";
	$result = mysqli_query($conn, $sql);
	echo "<h1>$pkgsrc $arch</h1>";
	if (mysqli_num_rows($result) > 0) 
	{
    		// Table start
		//echo "<table style=\"width:100%\" border=1>";
		echo "<table border=1>";
		echo "<tr>";
		echo "<th>ID</th>";
		echo "<th>Pkgsrc</th>";
		echo "<th>OS</th>";
		echo "<th>Arch</th>";
		echo "<th>Package</th>";
		echo "<th>Category</th>";
		echo "<th>Priority</th>";
		echo "<th>Status</th>";
		echo "</tr>";
		echo "<br>";
    		// output data of each row
    		while($row = mysqli_fetch_assoc($result)) 
		{
			echo "<tr>";
        		echo "<td>" . $row["no_package"] . "</td>";
        		echo "<td>" . $row["nom_source"] . "</td>";
        		echo "<td>" . $row["nom_os"] . "</td>";
        		echo "<td>" . $row["nom_arch"] . "</td>";
        		echo "<td>" . $row["nom_package"] . "</td>";
        		echo "<td>" . $row["category_package"] . "</td>";
        		echo "<td>" . $row["priority_package"] . "</td>";
        		echo "<td>" . $row["no_status"] . "</td>";
			echo "</tr>";
    		}
		echo "</table>";
	} 
	else 
	{
    		echo "0 results";
	}
}
function generateInputTable ($conn)
{
	$sql = "SELECT * from input;";
	$result = mysqli_query($conn, $sql);
	echo "<h1>INPUT</h1>";
	if (mysqli_num_rows($result) > 0)
	{
		echo "<table border=1>";
                echo "<tr>";
		echo "<th>Date</th>";
		echo "<th>Heure</th>";
		echo "<th>Temps du boire</th>";
		echo "<th>Quantite lait</th>";
		echo "<th>Source du lait</th>";
		echo "<th>Reveille par</th>";
		echo "</tr>";
		echo "<br>";
	while($row = mysqli_fetch_assoc($result))
                {
			echo "<tr>";
			echo "<td>" . $row["date"] . "</td>";
			echo "<td>" . $row["time"] . "</td>";
			echo "<td>" . $row["drink_time"] . "</td>";
			echo "<td>" . $row["milk_qt"] . "</td>";
			echo "<td>" . $row["milk_source"] . "</td>";
			echo "<td>" . $row["awaken_by"] . "</td>";
			echo "</tr>";
		}
		echo "</table>";
	}
}
function generateOutputTable ($conn)
{
        $sql = "SELECT * from output;";
        $result = mysqli_query($conn, $sql);
        echo "<h1>OUTPUT</h1>";
        if (mysqli_num_rows($result) > 0)
        {
                echo "<table border=1>";
                echo "<tr>";
                echo "<th>Date</th>";
                echo "<th>Heure</th>";
                echo "<th>Caca</th>";
                echo "<th>Pipi</th>";
                echo "</tr>";
                echo "<br>";
        while($row = mysqli_fetch_assoc($result))
                {
			echo "<tr>";
                	echo "<td>" . $row["date"] . "</td>";
                	echo "<td>" . $row["time"] . "</td>";
                	echo "<td>" . $row["poo"] . "</td>";
                	echo "<td>" . $row["pee"] . "</td>";
                	echo "</tr>";
                }
		echo "</table>";
        }
}

$conn = connect("$servername", "$username", "$password", "$dbname");
generateInputTable ($conn);
generateOutputTable ($conn);
disconnect($conn);
?>
